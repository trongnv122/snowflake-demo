# TEST PROJECT FOR SNOWFLAKE DATA CLOUD

### SETUP 
    - JDK 8
    - Spring boot 2.7.3
    - Hibernate, Lombok
### Goals:
    - Connect to SnowFlake data cloud
    - Test less cost of maintenance data growth up
    - Check compatibility of SnowFlake with data event sourcing

### INIT DATA FOR TESTING
```

truncate USERS;

INSERT INTO USERS (id, username, display_name)
WITH
    L0   AS (SELECT c FROM (SELECT 1 UNION ALL SELECT 1 UNION ALL
                            SELECT 1 UNION ALL SELECT 1 UNION ALL
                            SELECT 1 UNION ALL SELECT 1) AS D(c)), -- 6^1
    L1   AS (SELECT 1 AS c FROM L0 AS A CROSS JOIN L0 AS B),       -- 6^2
    L2   AS (SELECT 1 AS c FROM L1 AS A CROSS JOIN L1 AS B),       -- 6^4
    L3   AS (SELECT 1 AS c FROM L2 AS A CROSS JOIN L2 AS B),       -- 6^8
    Nums AS (SELECT ROW_NUMBER() OVER(ORDER BY (SELECT NULL)) AS k FROM L3)
select k as id , concat('a_', cast (k as varchar)) as USERNAME, concat('b_', cast (k/2 as varchar)) as DISPLAY_NAME
from nums
where k <= 1000000;

-- for now, test app only applied with 1679616 records

INSERT INTO ORDERS (id, user_id, order_name, AMOUNT)
WITH
    L0   AS (SELECT c FROM (SELECT 1 UNION ALL SELECT 1 UNION ALL
                            SELECT 1 UNION ALL SELECT 1 UNION ALL
                            SELECT 1 UNION ALL SELECT 1) AS D(c)), -- 6^1
    L1   AS (SELECT 1 AS c FROM L0 AS A CROSS JOIN L0 AS B),       -- 6^2
    L2   AS (SELECT 1 AS c FROM L1 AS A CROSS JOIN L1 AS B),       -- 6^4
    L3   AS (SELECT 1 AS c FROM L2 AS A CROSS JOIN L2 AS B),       -- 6^8
    Nums AS (SELECT ROW_NUMBER() OVER(ORDER BY (SELECT NULL)) AS k FROM L3)
select k as id , k as user_id , concat('ORD_', cast (k as varchar)) as ORDER_NAME, k as AMOUNT
from nums
where k <= 100000000 ;

```

### NOTES
```
create empty class and put into application.properties com.example.snowflake.EmptyDialect
```

https://docs.google.com/presentation/d/1kkjPpF8nFKLXyTo2eki-_4U81d_XAOGMx0JvLnzq0lc/edit?usp=sharing