package com.example.snowflake.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data

public class OrderReport {
    private String username;
    private String displayName;

    private Long orderCount;
    private BigDecimal totalAmount;

    public OrderReport() {
    }

    public OrderReport(String username, String displayName, Long orderCount, BigDecimal totalAmount) {
        this.username = username;
        this.displayName = displayName;
        this.orderCount = orderCount;
        this.totalAmount = totalAmount == null ? BigDecimal.ZERO : totalAmount;
    }
}
