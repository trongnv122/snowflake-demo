package com.example.snowflake.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Data
@Table(name = "ATTACHMENTS")
@Entity
public class AttachmentEntity extends BaseEntity{
    @Column(columnDefinition = "BINARY")
    private byte[] data;
}
