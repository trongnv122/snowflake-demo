package com.example.snowflake.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = true)
@Data
@Table(name = "orders")
@Entity
public class OrderEntity extends BaseEntity{

    @Column(name = "username")
    private String username;

    @Column(name = "CUSTOMER_DISPLAY_NAME")
    private String customerDisplayName;

    @Column(name = "ORDER_NAME")
    private String orderName;

    @Column(name = "AMOUNT")
    private BigDecimal amount;
}
