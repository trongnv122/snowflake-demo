package com.example.snowflake.controller;

import com.example.snowflake.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/report-order")
public class OrderReportController {

    @Autowired
    private OrderRepository orderRepository;

    @GetMapping
    public ResponseEntity<?> getReport() {
        return ResponseEntity.ok(orderRepository.getReport());
    }
}
