package com.example.snowflake.controller;

import com.example.snowflake.entity.AttachmentEntity;
import com.example.snowflake.repository.AttachmentRepository;
import net.snowflake.client.jdbc.internal.apache.commons.io.FileUtils;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RestController
public class UploadController {
    @Autowired
    private AttachmentRepository repository;
    private ExecutorService executor = Executors.newFixedThreadPool(3);

    @PostMapping("/upload")
    public ResponseEntity<?> uploadFile(@RequestPart MultipartFile file) throws IOException {
        File fileTarget = new File("D:\\sources\\",
                Objects.requireNonNull(file.getOriginalFilename()));

        FileCopyUtils.copy(file.getInputStream(), Files.newOutputStream(fileTarget.toPath()));

        this.uploadSF(fileTarget);
        return ResponseEntity.ok("Saved");
    }

    private void uploadSF(File file) {
        executor.submit(() -> {
            byte[] encoded;
            try {
                encoded = Base64.encodeBase64(FileUtils.readFileToByteArray(file));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            AttachmentEntity attachmentEntity = new AttachmentEntity();
            attachmentEntity.setData(encoded);
            repository.save(attachmentEntity);
        });

    }


}
