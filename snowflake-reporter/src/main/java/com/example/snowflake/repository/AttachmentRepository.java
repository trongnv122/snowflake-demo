package com.example.snowflake.repository;


import com.example.snowflake.entity.AttachmentEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AttachmentRepository extends CrudRepository<AttachmentEntity, Long> {

}
