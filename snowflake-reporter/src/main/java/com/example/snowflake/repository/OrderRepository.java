package com.example.snowflake.repository;


import com.example.snowflake.dto.OrderReport;
import com.example.snowflake.entity.OrderEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends CrudRepository<OrderEntity, Long> {

    @Query("SELECT new com.example.snowflake.dto.OrderReport(" +
            "u.username, u.displayName, COUNT(o.username), SUM(o.amount)) " +
            "FROM UserEntity u " +
            "LEFT JOIN OrderEntity o ON u.username = o.username " +
            "GROUP BY u.username, u.displayName ORDER BY u.username ")
    List<OrderReport> getReport();
}
