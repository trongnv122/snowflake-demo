# Setting up Database included:
- This section provide setting for database, include [Postgres](Postgres) and [MongoDB](MongoDB).
- Use docker stack for deploying

## How to run
- Set your AWS credential in .aws file.
- Execute run.sh for deploy

## Postgres
- version 14.6.
- Use [Dockerfile_postgres](Dockerfile_postgres) to build image for docker stack deploy. 
- This image include some installation which enable consumer via Python script and push WAL message to Kinesis Stream
- Enable Postgres CDC with logical replicate by running [init_wal2json.sql](init_wal2json.sql)


## MongoDB
- version 4.0-xenial.
- Set to replication with 3 nodes by commands in run.sh 
