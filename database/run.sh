docker build -f ./Dockerfile_postgres -t snowflake_postgres:1.0 .

docker stack deploy -c ./mongodb.yml snowflake-demo

sleep 5

echo -e "\nConfiguring the MongoDB ReplicaSet.\n"
declare mongo1_container=$(docker ps -q -f name=snowflake-demo_mongo1)

declare response=$(docker exec $mongo1_container  /usr/bin/mongo --eval "" | grep "MongoDB server version:")
echo "MongoDB response: $response"

while [[  $response != *"MongoDB server version:"*  ]] ; do
    echo "Wait for 5 seconds"
    sleep 5
done

docker exec $(docker ps -q -f name=snowflake-demo_mongo1)  /usr/bin/mongo --eval '''if (rs.status()["ok"] == 0) {
        rsconf = {
          _id : "rs0",
          members: [
            { _id : 0, host : "mongo1:27017", priority: 1.0 },
            { _id : 1, host : "mongo2:27017", priority: 0.5 },
            { _id : 2, host : "mongo3:27017", priority: 0.5 }
          ]
        };
        rs.initiate(rsconf);
    }
    rs.conf();'''


docker stack deploy -c postgres.yml snowflake-demo
docker service update  --force snowflake-demo_postgres
docker exec $(docker ps -q -f name=snowflake-demo_postgres. | head -n1) /bin/bash /configs/entrypoint.sh

echo 'Init postgres done'
