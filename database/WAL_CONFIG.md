- Show wal support level
```show wal_level; ```


- Show Replication slot support
``` show max_replication_slots;```


- Create role
``` create user repluser password 'replpass';```

- Create File format for external stage in Snowflake
- CREATE FILE FORMAT <name>
  TYPE = 'CSV'
  FIELD_DELIMITER = '|'
  SKIP_HEADER = 1;

- Create Kinesis demo IAM user with AmazonKinesisFullAccess
- Create Kinesess with provision option 1 shard