import boto3
import json
import time
import psycopg2
from psycopg2.extras import LogicalReplicationConnection
import syslog

# time.sleep(5)


my_stream_name = 'snowflake_demo_stream'
kinesis_client = boto3.client('kinesis', region_name='ap-southeast-1')
my_connection  = psycopg2.connect(
                   "dbname='customer' host='localhost' user='demo-user' password='changeme'" ,
                   connection_factory = LogicalReplicationConnection)
cur = my_connection.cursor()

try:
    cur.drop_replication_slot('wal2json_test_slot')
except:
    syslog.syslog("drop_replication_slot can not execute")

cur.create_replication_slot('wal2json_test_slot', output_plugin = 'wal2json')


cur.start_replication(slot_name = 'wal2json_test_slot', options = {'pretty-print' : 1}, decode= True)

def consume(msg):
    output = str(json.dumps(msg.payload).replace('\\t', '').replace('\\n', '').replace('\\"', '"'))
    kinesis_client.put_record(StreamName=my_stream_name, Data=output[1:][:-1], PartitionKey="default")
    print (output[1:][:-1])

cur.consume_stream(consume)