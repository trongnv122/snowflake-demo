#!/bin/bash

while ! pg_isready
do
    echo "$(date) - waiting for database to start"
    sleep 5
done

nohup python3.9 -u /configs/pip_wal2json_stream.py > /configs/pip_wal2json_stream.log &
sleep 2
echo "Run pip_wal2json_stream done"
nohup python3.9 -u /configs/pip_wal2json_stream_orders.py > /configs/pip_wal2json_stream_orders.log &
sleep 2
echo "Run pip_wal2json_stream_orders done"
