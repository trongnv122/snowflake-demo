-- ONLY INIT AT THE FIRST TIME WE START DB

CREATE PUBLICATION cdc;

select pg_drop_replication_slot('wal2json_test_slot');
SELECT pg_create_logical_replication_slot('wal2json_test_slot', 'wal2json');

ALTER PUBLICATION cdc ADD TABLE users;