set ts1 = (select current_timestamp());

set ts1 = (select '2022-12-19 23:49:08.430'::timestamp);

alter table USERS set change_tracking = true;

INSERT INTO DEMO.PUBLIC.USERS
    (USERNAME, PASSWORD, DISPLAY_NAME, FIRST_NAME, LAST_NAME)
VALUES ('th4', '', 'Tom4 Harry4', 'Tom4', 'Harry4');

UPDATE DEMO.PUBLIC.USERS SET LAST_NAME = 'Harry41' WHERE USERNAME = 'th4';


select * from USERS changes(information => default) at(timestamp => $ts1);

?? CHecking for update history was not shown