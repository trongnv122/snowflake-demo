create or replace table monthly_sales(empid int, amount int, month text)
as select * from values
                     (1, 10000, 'JAN'),
                     (1, 400, 'JAN'),
                     (2, 4500, 'JAN'),
                     (2, 35000, 'JAN'),
                     (1, 5000, 'FEB'),
                     (1, 3000, 'FEB'),
                     (2, 200, 'FEB'),
                     (2, 90500, 'FEB'),
                     (1, 6000, 'MAR'),
                     (1, 5000, 'MAR'),
                     (2, 2500, 'MAR'),
                     (2, 9500, 'MAR'),
                     (1, 8000, 'APR'),
                     (1, 10000, 'APR'),
                     (2, 800, 'APR'),
                     (2, 4500, 'APR');


select *
from monthly_sales
         pivot(sum(amount) for month in ('JAN', 'FEB', 'MAR', 'APR'))
         as p
order by empid;


-- UNPIVOT

-- example setup
create or replace table up_monthly_sales(empid int, dept text, jan int, feb int, mar int, april int);

insert into up_monthly_sales values
                              (1, 'electronics', 100, 200, 300, 100),
                              (2, 'clothes', 100, 300, 150, 200),
                              (3, 'cars', 200, 400, 100, 50);

-- UNPIVOT example
explain select * from up_monthly_sales
    unpivot(sales for month in (jan, feb, mar, april))
order by empid;