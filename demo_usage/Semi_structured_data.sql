create or replace table array_example (json variant);

insert into array_example (json)
select array_construct(12, 'twelve', null);

insert into array_example  (JSON)  (SELECT object_construct('Alberta', 'Edmonton', 'Manitoba', 'Winnipeg'));

select object_construct('Alberta', 'Edmonton', 'Manitoba', 'Winnipeg');

insert into array_example VALUES ('')

select *
from array_example;

-- Accessing Elements of an OBJECT by Key
select json['Manitoba'] as Manitoba,
       json:Alberta as Alberta
from array_example  ae where json ;

create or replace table test_semi_structured(var variant,
                                             arr array,
                                             obj object
);

desc table test_semi_structured;

-- object_construct