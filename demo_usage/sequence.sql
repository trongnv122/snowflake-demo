-- sequence like other db
-- Dont use currval



-- Sample of sequence in table
create sequence PUBLIC.USER_ID_SEQ
    start with 1;

create table IF NOT EXISTS PUBLIC.ORDERS
(
    ID           NUMBER default PUBLIC.USER_ID_SEQ.nextval
        primary key,
    USERNAME     VARCHAR(200),
    PASSWORD     VARCHAR(200),
    FIRSTNAME     VARCHAR(200),
    LASTNAME     VARCHAR(200),
    DISPLAY_NAME VARCHAR(200)
);


-- Use sequence like table
select * from
    (
        select f1.value u_value, f2.value p_value, f2.index user_index, u_seq.nextval u_next,
               p_seq.nextval p_next
        from input, lateral flatten(input.json) f1, table(getnextval(USERS_SEQ)) u_seq,
             lateral flatten(f1.value:contacts) f2, table(getnextval(PROFILES_SEQ)) p_seq
    );

