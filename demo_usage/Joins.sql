
with W_SQL as (
    SELECT u.DISPLAY_NAME, U.FIRST_NAME, U.LAST_NAME FROM USERS u
)
SELECT DISPLAY_NAME, FIRST_NAME, LAST_NAME FROM W_SQL;

select u.id, u.DISPLAY_NAME, p_email.DATA, p_phone.DATA
from USERS as u
    join PROFILES as p_email on p_email.U_ID = u.id AND p_email.TYPE = 'email'
    join PROFILES as p_phone on p_phone.U_ID = u.id AND p_phone.TYPE = 'phone'
order by u.id, u.DISPLAY_NAME;