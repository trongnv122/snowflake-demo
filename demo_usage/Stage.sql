-- List file of stage table
list @%snowflake;

-- View stage table data
select * from @%snowflake;

-- Cleanup table
rm @%snowflake;