--Add public key for user
alter user TRONGNV20 set rsa_public_key='your_public_key_exclude_header_and_footer';

--Check for public key added
desc user TRONGNV20;

-- Check existing data of snowflake target table
select * FROM  snowflake;

-- Cleanup if needed
truncate snowflake;

-- Create Snowpipe for transform from stage file into stage table
create or replace pipe DEMO.PUBLIC.SNOWFLAKE_KAFKA_CONNECTOR_MY_FAVORITE_CELEBRITIES_SINK_SNOWFLAKE_PIPE_SNOWFLAKE_0 auto_ingest = false
    as COPY INTO (select $1:meta, $1:content
    from @SNOWFLAKE_KAFKA_CONNECTOR_MY_FAVORITE_CELEBRITIES_SINK_SNOWFLAKE_STAGE_snowflake t) file_format = (type = 'json');


-- List file of stage table
list @%snowflake;

-- View stage table data
select * from @%snowflake;

-- Cleanup table
rm @%snowflake;


-- Create task with schedule in 5 minutes
CREATE TASK copy_state_table_into_target
  WAREHOUSE = COMPUTE_WH
  SCHEDULE = '5 MINUTE'
AS
copy into snowflake(RECORD_METADATA, RECORD_CONTENT) from (select $1:name, $1:email from @%snowflake t) file_format = (type = 'json');

-- Run immediately
EXECUTE TASK copy_state_table_into_target;

-- Recurring task
select system$task_dependents_enable('SF_TRONGNV122_DEMO.PUBLIC.copy_state_table_into_target');

