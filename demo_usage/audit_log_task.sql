create or replace table PUBLIC.ORDERS
(
    ID                    NUMBER not null primary key,
    AMOUNT                NUMBER(19, 2),
    CUSTOMER_DISPLAY_NAME VARCHAR(255),
    ORDER_NAME            VARCHAR(255),
    USERNAME              VARCHAR(255)
);



CREATE OR REPLACE PROCEDURE SP_PARSE_AUDIT_LOG()
    RETURNS VARCHAR
    LANGUAGE JAVASCRIPT
AS
$$
var num_rows_sql = "SELECT ID, DATA FROM INPUT WHERE STATUS = 0";
var stmt = snowflake.createStatement( {sqlText: num_rows_sql} );
var rows_result = stmt.execute();


while(rows_result.next()) {
    var streamItem = rows_result.getColumnValue(2);
    streamItem = '[' +streamItem.replaceAll(/}{/g, "},{")+ ']';

    // snowflake.createStatement( { sqlText: `INSERT INTO INPUT_TRANFORM_1 SELECT ?::VARIANT`,
    //     binds: [streamItem] } ).execute();

    var value_array = JSON.parse(streamItem);
    let targetTable = [];

    for (var i = 0; i < value_array.length; i ++ ) {
        let change = value_array[i]['change'];


        if (!change) continue;

        for (var j = 0; j < change.length; j ++ ) {
            let changeItem = change[j];

            let table = changeItem['table'];
            if (targetTable.indexOf(table) < 0)
                targetTable.push(table);

            if (!changeItem || !changeItem['kind']) continue;

            if (changeItem['kind'] === 'delete') {
                let deleteId = changeItem['oldkeys']['keyvalues'][changeItem['oldkeys']['keynames'].indexOf("id")];
                snowflake.createStatement( { sqlText: `DELETE FROM ${table} WHERE ID = ?`,
                    binds: [deleteId] } ).execute();

            }
            else if (changeItem['kind'] === 'insert' && table == 'users') {
                let ID = changeItem['columnvalues'][changeItem['columnnames'].indexOf("id")];
                let USERNAME = changeItem['columnvalues'][changeItem['columnnames'].indexOf("username")];
                let FIRST_NAME = changeItem['columnvalues'][changeItem['columnnames'].indexOf("first_name")];
                let LAST_NAME = changeItem['columnvalues'][changeItem['columnnames'].indexOf("last_name")];
                let DISPLAY_NAME = changeItem['columnvalues'][changeItem['columnnames'].indexOf("display_name")];

                snowflake.createStatement( { sqlText: `INSERT INTO USERS(ID, USERNAME, FIRST_NAME, LAST_NAME, DISPLAY_NAME)
                        VALUES (?,?,?,?,?)`,
                    binds: [ID, USERNAME, FIRST_NAME, LAST_NAME, DISPLAY_NAME] } ).execute();
            }
            else if (changeItem['kind'] === 'update' && table == 'users') {
                let ID = changeItem['oldkeys']['keyvalues'][changeItem['oldkeys']['keynames'].indexOf("id")];
                let USERNAME = changeItem['columnvalues'][changeItem['columnnames'].indexOf("username")];
                let FIRST_NAME = changeItem['columnvalues'][changeItem['columnnames'].indexOf("first_name")];
                let LAST_NAME = changeItem['columnvalues'][changeItem['columnnames'].indexOf("last_name")];
                let DISPLAY_NAME = changeItem['columnvalues'][changeItem['columnnames'].indexOf("display_name")];

                snowflake.createStatement( { sqlText:
                    'MERGE INTO USERS ' +
                            'USING (SELECT ID FROM USERS WHERE ID = ? ' +
                    ') s ' +
                    'ON  S.ID = USERS.ID ' +
                    'WHEN MATCHED THEN UPDATE SET USERNAME=?, FIRST_NAME=?, LAST_NAME=?, DISPLAY_NAME=? ' +
                    'WHEN NOT MATCHED THEN INSERT (ID, USERNAME, FIRST_NAME, LAST_NAME, DISPLAY_NAME) VALUES (?,?,?,?,?); ',
                    binds: [ID, USERNAME, FIRST_NAME, LAST_NAME, DISPLAY_NAME,
                        ID, USERNAME, FIRST_NAME, LAST_NAME, DISPLAY_NAME] } ).execute();
            }
            else if (changeItem['kind'] === 'insert' && table === 'orders') {
                let ID = changeItem['columnvalues'][changeItem['columnnames'].indexOf("id")];
                let USERNAME = changeItem['columnvalues'][changeItem['columnnames'].indexOf("username")];
                let ORDER_NAME = changeItem['columnvalues'][changeItem['columnnames'].indexOf("order_name")];
                let CUSTOMER_DISPLAY_NAME = changeItem['columnvalues'][changeItem['columnnames'].indexOf("customer_display_name")];
                let AMOUNT = changeItem['columnvalues'][changeItem['columnnames'].indexOf("amount")];

                snowflake.createStatement( { sqlText: `INSERT INTO ORDERS(ID, USERNAME, CUSTOMER_DISPLAY_NAME, ORDER_NAME, AMOUNT)
                        VALUES (?,?,?,?,?)`,
                    binds: [ID, USERNAME, CUSTOMER_DISPLAY_NAME, ORDER_NAME, AMOUNT] } ).execute();
            }
            else if (changeItem['kind'] === 'update' && table === 'orders') {
                let ID = changeItem['oldkeys']['keyvalues'][changeItem['oldkeys']['keynames'].indexOf("id")];
                let USERNAME = changeItem['columnvalues'][changeItem['columnnames'].indexOf("username")];
                let ORDER_NAME = changeItem['columnvalues'][changeItem['columnnames'].indexOf("order_name")];
                let CUSTOMER_DISPLAY_NAME = changeItem['columnvalues'][changeItem['columnnames'].indexOf("customer_display_name")];
                let AMOUNT = changeItem['columnvalues'][changeItem['columnnames'].indexOf("amount")];

                snowflake.createStatement( { sqlText:
                    'MERGE INTO ORDERS ' +
                        'USING (SELECT ID FROM ORDERS WHERE ID = ? ' +
                        ') s ' +
                        'ON  S.ID = ORDERS.ID ' +
                        'WHEN MATCHED THEN UPDATE SET USERNAME=?, ORDER_NAME=?, CUSTOMER_DISPLAY_NAME=?, AMOUNT=? ' +
                        'WHEN NOT MATCHED THEN INSERT (ID, USERNAME, ORDER_NAME, CUSTOMER_DISPLAY_NAME, AMOUNT) VALUES (?,?,?,?,?); ',
                    binds: [ID, USERNAME, ORDER_NAME, CUSTOMER_DISPLAY_NAME, AMOUNT, ID,
                        USERNAME, ORDER_NAME, CUSTOMER_DISPLAY_NAME, AMOUNT] } ).execute();
            }
        }
    }


    snowflake.createStatement( { sqlText: "UPDATE INPUT SET STATUS = 1, TARGET_TABLE=? WHERE ID = ? AND STATUS = 0",
        binds: [targetTable.join(' '), rows_result.getColumnValue(1)] } ).execute();

}
return 'OK';
$$;

-- Create task with schedule in 5 minutes
CREATE OR REPLACE TASK copy_state_table_into_target
    WAREHOUSE = COMPUTE_WH
    SCHEDULE = '1 MINUTE'
    AS
        CALL ADD_OBSERVATION_VALUES();
--
-- -- Create task with schedule in 5 minutes
-- CREATE OR REPLACE TASK copy_state_table_into_target_order
--     WAREHOUSE = COMPUTE_WH
--     SCHEDULE = '1 MINUTE'
--     AS
--         CALL ADD_OBSERVATION_VALUES_ORDERS();

-- Run immediately
EXECUTE TASK copy_state_table_into_target;
-- EXECUTE TASK copy_state_table_into_target_order;

-- Recurring task
select system$task_dependents_enable('DEMO.PUBLIC.copy_state_table_into_target');
-- select system$task_dependents_enable('DEMO.PUBLIC.copy_state_table_into_target_order');