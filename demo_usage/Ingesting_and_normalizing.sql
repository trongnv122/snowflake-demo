-- Ingesting and Normalizing Denormalized Data
-- Ingesting and Normalizing Denormalized Data
create table IF NOT EXISTS PUBLIC.USERS
(
    ID           NUMBER default PUBLIC.USER_ID_SEQ.nextval
        primary key,
    USERNAME     VARCHAR(200),
    PASSWORD     VARCHAR(200),
    FIRSTNAME     VARCHAR(200),
    LASTNAME     VARCHAR(200),
    DISPLAY_NAME VARCHAR(200)
);


create table IF NOT EXISTS PROFILES (id number, u_id number, type string, data string);

-- sequences to produce primary keys on our data tables

create or replace sequence USERS_SEQ;
create or replace sequence PROFILES_SEQ;

-- staging table for json

create or replace table input (json variant);


-- Insert Data
insert into input select parse_json(
                                 '[
                                  {
                                    username : \'jd1\',
                                    firstName : \'john\',
                                    lastName : \'doe\',
                                    contacts : [
                                      {
                                        contactType : \'phone\',
                                        contactData : \'1234567890\',
                                      }
                                      ,
                                      {
                                        contactType : \'email\',
                                        contactData : \'jdoe@acme.com\',
                                      }
                                     ]
                                    }
                                 ,
                                   {
                                    username : \'sm1\',
                                    firstName : \'mister\',
                                    lastName : \'smith\',
                                    contacts : [
                                      {
                                        contactType : \'phone\',
                                        contactData : \'0987654321\',
                                      }
                                      ,
                                      {
                                        contactType : \'email\',
                                        contactData : \'msmith@acme.com\',
                                      }
                                      ]
                                    }
                                 ]'
                             );

insert all
    when 1=1 then
        into PROFILES values (p_next, u_next, p_value:contactType, p_value:contactData)
    when user_index = 0 then
        into USERS values (u_next, u_value:username, '', CONCAT(u_value:firstName, ' ', u_value:lastName), u_value:firstName, u_value:lastName)

select * from
    (
        select f1.value u_value, f2.value p_value, f2.index user_index, u_seq.nextval u_next,
               p_seq.nextval p_next
        from input, lateral flatten(input.json) f1, table(getnextval(USERS_SEQ)) u_seq,
             lateral flatten(f1.value:contacts) f2, table(getnextval(PROFILES_SEQ)) p_seq
    );




insert all
    when 1=1 then
        into PROFILES values (p_next, u_next, p_value:contactType, p_value:contactData)
    when user_index = 0 then
        into USERS values (u_next, u_value:username, '', CONCAT(u_value:firstName, ' ', u_value:lastName), u_value:firstName, u_value:lastName)

select * from
    (
        select f1.value u_value, f2.value p_value, f2.index user_index, USERS_SEQ.nextval u_next,
               PROFILES_SEQ.nextval p_next
        from input, lateral flatten(input.json) f1,
             lateral flatten(f1.value:contacts) f2
    );


????Tranform file into binary and store into Snowflake