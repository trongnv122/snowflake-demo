
-- Selecting All Columns Except One Column
select * exclude U_ID from PROFILES;

-- Selecting All Columns Except Two or More Columns
select * exclude (USERNAME, PASSWORD) from USERS;

describe table USERS;

