package com.example.snowflake;


import io.github.cdimascio.dotenv.Dotenv;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class SimpleStatementsApi {
	public static void main(String args[]) throws SQLException {
		String sql = args[0];

		System.out.println("sql >>>>>> " + sql);

		Dotenv dotenv = Dotenv.configure()
				.ignoreIfMalformed()
				.ignoreIfMissing()
				.load();
		Properties properties = new Properties();
		properties.put("user", dotenv.get("snowflake_user_name"));
		properties.put("password", dotenv.get("snowflake_password"));
		properties.put("account", dotenv.get("snowflake_account_name"));
		properties.put("warehouse", dotenv.get("snowflake_warehouse"));
		properties.put("db", dotenv.get("snowflake_database_name"));
		properties.put("schema", dotenv.get("snowflake_schema_name"));
		properties.put("role",dotenv.get("snowflake_user_role"));

		//JDBC connection string
		String jdbcUrl = "jdbc:snowflake://" + dotenv.get("snowflake_url_name");
		if (!jdbcUrl.endsWith("/")) jdbcUrl+= "/";

		System.out.println("Created JDBC connection");
		Connection connection = DriverManager.getConnection(jdbcUrl, properties);
		System.out.println("Done creating JDBC connection");

		System.out.println("Created JDBC statement");
		Statement statement = connection.createStatement();

		// create a table
		System.out.println("Execute snowflake statement");
		statement.executeUpdate(sql);
		statement.close();
		System.out.println("Execute snowflake statement DONE");

		connection.close();
		System.out.println("End connection");
	}
}