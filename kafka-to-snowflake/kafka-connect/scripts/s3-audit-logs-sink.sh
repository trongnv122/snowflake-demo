#!/bin/sh

echo -e "\n\CLEAN UP AUDIT_LOGS_SINK_S3 connector\n\n"
curl --output /dev/null -X DELETE http://localhost:8083/connectors/AUDIT_LOGS_SINK_S3 || true

echo -e "\n\SETTING UP AUDIT_LOGS_SINK_S3 connector\n\n"
curl -i -X PUT -H  "Content-Type:application/json" \
    http://localhost:8083/connectors/AUDIT_LOGS_SINK_S3/config \
    -d '{
        "name": "AUDIT_LOGS_SINK_S3",
        "topics": "mongo.audit_logs",
        "connector.class": "io.confluent.connect.s3.S3SinkConnector",
        "key.converter": "org.apache.kafka.connect.converters.ByteArrayConverter",
        "value.converter": "org.apache.kafka.connect.converters.ByteArrayConverter",
        "format.class": "io.confluent.connect.s3.format.bytearray.ByteArrayFormat",
        "flush.size": "1",
        "s3.bucket.name": "snowflake-demo-stage-bu9",
        "s3.region": "ap-southeast-1",
        "s3.credentials.provider.class": "io.aiven.kafka.connect.util.AivenAWSCredentialsProvider",
        "storage.class": "io.confluent.connect.s3.storage.S3Storage",
        "s3.credentials.provider.secret_access_key": "AKIAU5AZYKRJKEQI53BW",
        "s3.credentials.provider.access_key_id": "rzYKQiP5pKHR/p8Hh6z6QZxwF8OZn1f/a5F4R3Zt"
    }'
