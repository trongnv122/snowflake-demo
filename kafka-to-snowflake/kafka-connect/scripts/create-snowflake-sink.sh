#!/bin/sh

echo -e "\n\CLEAN UP AUDIT_LOGS_SINK_SNOWFLAKE connector\n\n"
curl --output /dev/null -X DELETE http://localhost:8083/connectors/AUDIT_LOGS_SINK_SNOWFLAKE || true

echo -e "\n\SETTING UP AUDIT_LOGS_SINK_SNOWFLAKE connector\n\n"
curl -i -X PUT -H  "Content-Type:application/json" \
    http://localhost:8083/connectors/AUDIT_LOGS_SINK_SNOWFLAKE/config \
    -d '{
        "connector.class":"com.snowflake.kafka.connector.SnowflakeSinkConnector",
        "tasks.max":1,
        "topics":"mongo.audit_logs",
        "snowflake.topic2table.map": "mongo.audit_logs:audit_logs",
        "snowflake.url.name": "${file:/opt/confluent/secrets/connect-secrets.properties:snowflake_url_name}",
        "snowflake.user.name": "${file:/opt/confluent/secrets/connect-secrets.properties:snowflake_user_name}",
        "snowflake.user.role": "${file:/opt/confluent/secrets/connect-secrets.properties:snowflake_user_role}",
        "snowflake.private.key": "${file:/opt/confluent/secrets/connect-secrets.properties:snowflake_private_key}",
        "snowflake.database.name": "${file:/opt/confluent/secrets/connect-secrets.properties:snowflake_database_name}",
        "snowflake.schema.name": "${file:/opt/confluent/secrets/connect-secrets.properties:snowflake_schema_name}",
        "key.converter":"org.apache.kafka.connect.storage.StringConverter",
        "value.converter.schemas.enable": "true",
        "value.converter":"com.snowflake.kafka.connector.records.SnowflakeJsonConverter",
        "value.converter.schema.registry.url":"http://schema-registry:8081"
    }'


#cp /opt/confluent/secrets/connect-secrets.properties /app/.env
#sql="
#  CREATE OR REPLACE PIPE
#      SNOWFLAKE_KAFKA_CONNECTOR_AUDIT_LOGS_SINK_SNOWFLAKE_PIPE_AUDIT_LOGS_0
#      auto_ingest = false as
#          copy into audit_logs (RECORD_METADATA, RECORD_CONTENT)
#              from (select $1, $1
#                    from @%audit_logs t) file_format = (type = 'json');
#"

#java -jar /app/run.jar %sql%