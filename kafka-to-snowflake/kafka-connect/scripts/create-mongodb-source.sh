#!/bin/sh


echo -e "\n\CLEAN UP\n\n"
curl --output /dev/null -X DELETE http://localhost:8083/connectors/EVENT_MONGO_DB_SOURCE || true

sleep 2
echo -e "\nAdding MongoDB Kafka Source Connector for the 'audit_logs' "

curl -i -X PUT -H  "Content-Type:application/json" \
    http://localhost:8083/connectors/EVENT_MONGO_DB_SOURCE/config \
    -d '{
                 "tasks.max":"1",
                 "connector.class":"com.mongodb.kafka.connect.MongoSourceConnector",
                 "connection.uri":"mongodb://mongo1:27017,mongo2:27017,mongo3:27017",
                 "key.converter": "org.apache.kafka.connect.json.JsonConverter",
                 "value.converter": "org.apache.kafka.connect.json.JsonConverter",
                 "topic.namespace.map": "{\"app_activities.audit_logs\" : \"audit_logs\"}",
                 "topic.prefix":"mongo",
                 "database":"app_activities",
                 "collection":"audit_logs"
        }'

sleep 2
echo -e "\nKafka Connectors: \n"
curl -X GET "http://localhost:8083/connectors/" -w "\n"
