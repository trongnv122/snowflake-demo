#!/bin/bash

docker build -t trongnv/kafka-connect:1.0 -f ./Dockerfile_kafka_connect .

docker service rm snowflake-demo_kafka-connect
docker service rm snowflake-demo_zookeeper
docker service rm snowflake-demo_broker


env $(cat ./.env | xargs) envsubst < ./docker-compose.yml | docker stack deploy --compose-file - snowflake-demo

echo 'Init kafka done'

echo 'connection is done'

nc -vz localhost 8083

