# snowflake-demo



## Getting started
```
This project arm to connect from microservice system to snowflake data cloud.
User can register, authenticate and create order. Microservices call each other to exchange data.
And on the other way, python shell will listen postgres database WAL changed and push into Kinesis. And then data will send to S3, Snowflake 
User can view report of order base on Snowflake with separate data instead of query from Postgres directly.

```
### Microservice
```
Microservices are based on Spring boot 2.7.0. They include:
- Config Server : Provide microservice environment variables
- Customer Service : User can register, authenticate, APIs resource for other services
- Order Service: User can order here, authen with token which created from Customer Service
- Report Service: View report of order from Snowflake database

```

### ENV
- Copy ````database/.aws.sample```` and rename to ```database/.aws```. Update your own settings
- Copy ````kafka-to-snowflake/.env.sample```` and rename to ```kafka-to-snowflake/.env```. Update your own settings
- Create local git repository for microservice profiles
  - ```mkdir -p /your-path/snowflake-demo-configs```
  - ```cd /your-path/snowflake-demo-configs```
  - ```git init```
  - ```touch customer-dev.properties```
  - ```touch order-dev.properties```
  - ```touch reporter-dev.properties```
  - ```git add -A```
  - ```git commit -m "Init config properties"```

### __How to run__
- Run [Kafka Message Service](Kafka):
  - ```#cd kafka-to-snowflake```
  - ```#source .env```
  - ```#docker-compose up -d```
- Run [Postgres](Postgres):
  - ```#cd database```
  - ```#./run.sh```

-  Run microservices 

