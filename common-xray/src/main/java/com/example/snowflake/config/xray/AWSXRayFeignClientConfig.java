package com.example.snowflake.config.xray;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.HttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.commons.httpclient.ApacheHttpClientFactory;
import org.springframework.cloud.openfeign.FeignFormatterRegistrar;
import org.springframework.cloud.openfeign.support.FeignHttpClientProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.datetime.standard.DateTimeFormatterRegistrar;

import com.amazonaws.xray.proxies.apache.http.HttpClientBuilder;

import feign.Client;
import feign.httpclient.ApacheHttpClient;

@Configuration(proxyBeanMethods = false)
@ConditionalOnClass(ApacheHttpClient.class)
@ConditionalOnMissingClass("com.netflix.loadbalancer.ILoadBalancer")
@ConditionalOnProperty(value = "feign.httpclient.enabled and aws-xray.enable", matchIfMissing = true)
public class AWSXRayFeignClientConfig {
    private final Logger log = LoggerFactory.getLogger(AWSXRayFeignClientConfig.class);

    @Bean
    public Client client(ApacheHttpClientFactory httpClientFactory,
                         HttpClientConnectionManager httpClientConnectionManager,
                         FeignHttpClientProperties httpClientProperties) {
        RequestConfig defaultRequestConfig = RequestConfig.custom()
                .setConnectTimeout(httpClientProperties.getConnectionTimeout())
                .setRedirectsEnabled(httpClientProperties.isFollowRedirects())
                .build();
        log.debug("Setting up aws xray Client bean for FeignClient.");
        return new ApacheHttpClient(
                HttpClientBuilder.create()
                        .setConnectionManager(httpClientConnectionManager)
                        .setDefaultRequestConfig(defaultRequestConfig)
                        .build()
        );
    }

    @Bean
    public FeignFormatterRegistrar localDateFeignFormatterRegister() {
        log.debug("Setting up FeignFormatterRegistrar bean for FeignClient");
        return registry -> {
            DateTimeFormatterRegistrar registrar = new DateTimeFormatterRegistrar();
            registrar.setUseIsoFormat(true);
            registrar.registerFormatters(registry);
        };
    }

}
