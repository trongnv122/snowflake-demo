##!/bin/bash
#
#
#### Build parent pom.
### Only build with stage = builder => cache parent artifact into Docker Cache

MODULES=();

### ARG: name, relative source path, profile, port
PARENT_PROFILES=("parent" "." "dev" "NONE")
COMMON_XRAY_PROFILES=("common-xray" "common-xray" "NONE" "NONE")
DISCOVERY_PROFILES=("discovery" "snowflake-discovery" "dev" "8504")
CONFIG_PROFILES=("config" "snowflake-config" "dev" "8500")
CUSTOMER_PROFILES=("customer" "snowflake-customer" "dev" "8501")
ORDER_PROFILES=("order" "snowflake-order" "dev" "8502")
REPORTER_PROFILES=("reporter" "snowflake-reporter" "dev" "8503")
LIQUIBASE_PROFILES=("snowflake-liquibase" "snowflake-liquibase" "dev" "8505")

MODULES=(
  PARENT_PROFILES[@]
  COMMON_XRAY_PROFILES[@]
  DISCOVERY_PROFILES[@]
  CONFIG_PROFILES[@]
  CUSTOMER_PROFILES[@]
  ORDER_PROFILES[@]
  REPORTER_PROFILES[@]
  LIQUIBASE_PROFILES[@]
)

ArrayLength=${#MODULES[@]}

for ((i=0; i<$ArrayLength; i++))
do
  echo ""
  echo ""

  name=${!MODULES[i]:0:1}
  src=${!MODULES[i]:1:1}
  active_profile=${!MODULES[i]:2:1}
  port=${!MODULES[i]:3:1}

  echo "build project with profile name: ${name}"
  echo "build project with profile src: ${src}"
  echo "build project with profile active_profile: ${active_profile}"
  echo "build project with profile port: ${port}"

  if [[ ${src} == *"pom.xml"* ]]
  then
     target_builder="--target builder"
     echo "Build only builder src = ${src}"
  else
    target_builder=""
  fi

    echo ">>>>>>>>>>>>> Build module ${name}"
    docker build -t trongnv/mics-${name}:1.0 \
       --build-arg MODULE=${name} --build-arg SRC=${src} --build-arg PROFILE=${active_profile} --build-arg PORT=${port} \
       -f ${src}/Dockerfile $target_builder .
    echo "<<<<<<<<<<<<<  DONE module ${name}"
    echo ""
    echo ""
done

docker service rm snowflake-demo_mics-discovery
docker service rm snowflake-demo_mics-config
docker service rm snowflake-demo_mics-customer
docker service rm snowflake-demo_mics-order
docker service rm snowflake-demo_mics-reporter


#health_check_and_wait () {
##  args : string url , integer w_time
#  while [[  $response != *'{"status":"UP"}'*  ]] ; do
#      echo "Wait for 5 seconds"
#      sleep ${2}
#      response=$(curl ${1} | grep '{"status":"UP"}')
#
#      echo "response     $response"
#  done
#}
#
echo $'\n\n<<<<<<<<<<<<< DEPLOY SERVER DISCOVERY  >>>>>>>>>>>'
docker stack deploy -c ./snowflake-discovery/docker-compose.yml snowflake-demo

echo $'\n\n<<<<<<<<<<<<< DEPLOY SERVER CONFIG  >>>>>>>>>>>'
env $(cat ./.env | xargs) envsubst < ./snowflake-config/docker-compose.yml | docker stack deploy -c - snowflake-demo

echo $'\n\n<<<<<<<<<<<<< DEPLOY SNOWFLAKE LIQUIBASE '
docker stack deploy -c ./snowflake-liquibase/docker-compose.yml snowflake-demo


echo $'\n\n<<<<<<<<<<<<< DEPLOY SERVER CUSTOMER  >>>>>>>>>>>'
docker stack deploy -c ./snowflake-customer/docker-compose.yml snowflake-demo


echo $'\n\n<<<<<<<<<<<<< DEPLOY SERVER ORDER  >>>>>>>>>>>'
docker stack deploy -c ./snowflake-order/docker-compose.yml snowflake-demo


echo $'\n\n<<<<<<<<<<<<< DEPLOY SERVER REPORTS >>>>>>>>>>>'
docker stack deploy -c ./snowflake-reporter/docker-compose.yml snowflake-demo


#docker service update --force snowflake-demo_mics-discovery
#docker service update --force snowflake-demo_mics-config
#docker service update --force snowflake-demo_mics-customer
#docker service update --force snowflake-demo_mics-order
#docker service update --force snowflake-demo_mics-reporter

#docker service logs -f snowflake-demo_mics-config
