FROM maven:3.5-jdk-8-alpine as builder

ENV HOME=/usr/app

ARG MODULE=$MODULE
ARG PORT=$PORT
ARG SRC=$SRC
ARG PROFILE=$PROFILE

RUN mkdir -p $HOME/${MODULE}

WORKDIR $HOME/${MODULE}

ADD pom.xml $HOME
#
COPY ${SRC} $HOME/${MODULE}

### CACHE .m2 for next build
RUN --mount=type=cache,target=/root/.m2 mvn -f $HOME/${MODULE}/pom.xml clean install -DskipTests

FROM tomcat:8.5.82-jdk8-openjdk-slim
ARG MODULE=$MODULE
ARG PROFILE=$PROFILE
ENV APP_PROFILE=$PROFILE

COPY --from=builder /usr/app/${MODULE}/target/*.jar /app/runner.jar

EXPOSE $PORT

ENTRYPOINT ["java", "-Dspring.profiles.active=${APP_PROFILE}", "-jar", "/app/runner.jar"]