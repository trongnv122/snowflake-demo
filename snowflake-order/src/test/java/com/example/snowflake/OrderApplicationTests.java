package com.example.snowflake;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrderApplication.class)
class OrderApplicationTests {
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	final String topicName = "snowflake";

	@Test
	public void testPublishMessage() {
		String message = "{\"name\":\"Trong001\",\"email\":\"trongnv20@fsoft.com.vn\"}";

		ListenableFuture<SendResult<String, String>> future =
				kafkaTemplate.send(topicName, message);

		future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {

			@Override
			public void onSuccess(SendResult<String, String> result) {
				System.out.println("Sent message=[" + message +
						"] with offset=[" + result.getRecordMetadata().offset() + "]");
			}
			@Override
			public void onFailure(Throwable ex) {
				System.out.println("Unable to send message=["
						+ message + "] due to : " + ex.getMessage());
			}
		});
	}

}
