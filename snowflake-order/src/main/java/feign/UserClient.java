package feign;

import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "jplaceholder", url = "https://jsonplaceholder.typicode.com/")
public class UserClient {
}
