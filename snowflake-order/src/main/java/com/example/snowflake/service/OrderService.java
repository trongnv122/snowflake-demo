package com.example.snowflake.service;

import com.example.snowflake.dto.OrderDTO;
import com.example.snowflake.dto.OrderRequest;
import com.example.snowflake.dto.UserDTO;
import com.example.snowflake.entity.OrderEntity;
import com.example.snowflake.feign.UserClient;
import com.example.snowflake.repository.OrderRepository;
import com.example.snowflake.utils.JwtUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserClient userClient;

    public OrderDTO placeOrder(OrderRequest request) {
        UserDTO userDTO = userClient.getUser(JwtUtils.getCurrentUserName());
        OrderEntity orderEntity = new OrderEntity();
        BeanUtils.copyProperties(request, orderEntity);
        orderEntity.setCustomerDisplayName(userDTO.getDisplayName());
        orderEntity.setUserName(userDTO.getUsername());
        orderEntity = orderRepository.save(orderEntity);

        orderEntity.setOrderName("#" + padLeftZeros(String.valueOf(orderEntity.getId()), 5));
        orderRepository.save(orderEntity);
        OrderDTO result = new OrderDTO();
        BeanUtils.copyProperties(orderEntity, result);
        return result;
    }

    private String padLeftZeros(String inputString, int length) {
        if (inputString.length() >= length) {
            return inputString;
        }
        StringBuilder sb = new StringBuilder();
        while (sb.length() < length - inputString.length()) {
            sb.append('0');
        }
        sb.append(inputString);

        return sb.toString();
    }
}
