package com.example.snowflake.controller;

import com.amazonaws.xray.spring.aop.XRayEnabled;
import com.example.snowflake.dto.OrderRequest;
import com.example.snowflake.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
@CrossOrigin
@XRayEnabled
public class OrderController {
    @Autowired
    private OrderService orderService;

    @PostMapping("/place-order")
    public ResponseEntity<?> getUser(@RequestBody OrderRequest request) {
        return ResponseEntity.ok(orderService.placeOrder(request));
    }
}
