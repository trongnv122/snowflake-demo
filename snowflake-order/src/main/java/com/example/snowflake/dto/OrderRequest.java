package com.example.snowflake.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class OrderRequest {
    private String orderName;
    private BigDecimal amount;
}
