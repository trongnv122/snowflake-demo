package com.example.snowflake.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class OrderDTO {
    private Long id;
    private String userName;
    private String customerDisplayName;
    private String orderName;
    private BigDecimal amount;
}
