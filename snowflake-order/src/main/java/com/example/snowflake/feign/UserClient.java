package com.example.snowflake.feign;

import com.example.snowflake.dto.UserDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "customer:8501")
public interface UserClient {

    @RequestMapping(method = RequestMethod.GET, value = "/user/{username}")
    UserDTO getUser(@PathVariable String username);

}