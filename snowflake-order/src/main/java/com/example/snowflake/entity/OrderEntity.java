package com.example.snowflake.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = true)
@Data
@Table(name = "orders")
@Entity
public class OrderEntity extends BaseEntity{

    @Column(name = "order_name")
    private String orderName;

    @Column(name = "customer_display_name")
    private String customerDisplayName;

    @Column
    private BigDecimal amount;

    @Column(name = "username")
    private String userName;
}
