CREATE TABLE IF NOT EXISTS ORDERS(
    id BIGSERIAL PRIMARY KEY,
    username VARCHAR(200),
    customer_display_name VARCHAR(200),
    order_name VARCHAR(200),
    amount DECIMAL
)