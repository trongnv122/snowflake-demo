CREATE TABLE IF NOT EXISTS USERS(
    id BIGSERIAL PRIMARY KEY,
    username VARCHAR(200),
    password VARCHAR(200),
    first_name VARCHAR(200),
    last_name VARCHAR(200),
    display_name VARCHAR(200)
)