package com.example.snowflake.service;

import com.example.snowflake.dto.AuditLog;
import com.example.snowflake.repository.AuditLogRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.security.core.Authentication;

import java.util.Date;
import java.util.UUID;

@Service
@Slf4j
public class AuditServiceImpl implements AuditService {
    public AuditServiceImpl(AuditLogRepository logRepository) {
        this.logRepository = logRepository;
    }

    private final AuditLogRepository logRepository;
    @Override
    public void createLog(Object data, String type) {
        try {
            AuditLog log = new AuditLog();
            log.setType(type);

            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

            log.setId(UUID.randomUUID().toString());
            log.setIssuer(authentication.getPrincipal().toString());
            log.setIssueAt(new Date());
            log.setData(data);

            logRepository.save(log);
        } catch (Exception ex) {
            log.error("Error occur while create log");
        }

    }
}
