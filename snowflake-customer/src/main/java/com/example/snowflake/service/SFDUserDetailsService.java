package com.example.snowflake.service;

import com.example.snowflake.dto.RegisterUserRequest;
import com.example.snowflake.dto.UserDTO;
import com.example.snowflake.entity.UserEntity;
import com.example.snowflake.repository.UserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class SFDUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findByUsername(username);

        if (userEntity != null) {
            return new User(userEntity.getUsername(), userEntity.getPassword(),
                    new ArrayList<>());
        } else {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
    }

    public UserDTO register (RegisterUserRequest req) {
        UserEntity userEntity = new UserEntity();
        BeanUtils.copyProperties(req, userEntity, "password");
        userEntity.setPassword(passwordEncoder.encode(req.getPassword()));
        userEntity = userRepository.save(userEntity);
        UserDTO response = new UserDTO();
        BeanUtils.copyProperties(userEntity, response);
        return response;
    }
}
