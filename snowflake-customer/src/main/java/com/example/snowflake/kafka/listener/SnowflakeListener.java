package com.example.snowflake.kafka.listener;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class SnowflakeListener {

    @KafkaListener(topics = "snowflake", groupId = "snowflake_group_id")
    public void listen(String message) {
        System.out.println("snowflake listening message: " + message);
    }
}
