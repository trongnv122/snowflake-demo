package com.example.snowflake.aop;

import com.example.snowflake.dto.JwtResponse;
import com.example.snowflake.dto.UserDTO;
import com.example.snowflake.entity.UserEntity;
import com.example.snowflake.repository.UserRepository;
import com.example.snowflake.service.AuditService;
import com.example.snowflake.service.JwtTokenUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;

@Aspect
@Configuration
public class ServiceAspect {
    private final AuditService auditService;
    private final JwtTokenUtil jwtTokenUtil;
    private final UserRepository userRepository;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public ServiceAspect(AuditService auditService, JwtTokenUtil jwtTokenUtil, UserRepository userRepository) {
        this.auditService = auditService;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userRepository = userRepository;
    }

    @AfterReturning(pointcut = "execution(* com.example.snowflake.controller.AuthenticationController.createAuthenticationToken(..))", returning = "result")
    public void after(JoinPoint joinPoint, Object result) {
        try {
            if (result instanceof ResponseEntity) {
                ResponseEntity<JwtResponse> response = (ResponseEntity<JwtResponse>) result;
                String token = response.getBody().getToken();
                String username = jwtTokenUtil.getUsernameFromToken(token);

                UserEntity userEntity = userRepository.findByUsername(username);
                UserDTO userDTO = new UserDTO();
                BeanUtils.copyProperties(userEntity, userDTO);
                auditService.createLog(userDTO, "authenticate");
            }
        } catch (Exception ex) {
            logger.error("Error occur while saving login audit log", ex);
        }

    }
}
