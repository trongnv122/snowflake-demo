package com.example.snowflake.controller.internal;

import com.example.snowflake.dto.UserDTO;
import com.example.snowflake.entity.UserEntity;
import com.example.snowflake.repository.UserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/{username}", method = RequestMethod.GET)
    public ResponseEntity<?> createAuthenticationToken(@PathVariable String username) throws Exception {

        UserEntity entity = userRepository.findByUsername(username);
        UserDTO dto = new UserDTO();
        BeanUtils.copyProperties(entity, dto);
        return ResponseEntity.ok(dto);
    }
}
