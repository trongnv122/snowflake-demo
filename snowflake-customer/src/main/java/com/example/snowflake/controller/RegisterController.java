package com.example.snowflake.controller;

import com.example.snowflake.dto.RegisterUserRequest;
import com.example.snowflake.service.SFDUserDetailsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin
public class RegisterController {
    private final SFDUserDetailsService userDetailsService;

    public RegisterController(SFDUserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody RegisterUserRequest request) throws Exception {

        return ResponseEntity.ok(userDetailsService.register(request));
    }

}
