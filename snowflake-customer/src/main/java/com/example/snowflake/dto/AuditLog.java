package com.example.snowflake.dto;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Document("audit_logs")
public class AuditLog {
    @Id
    private String id;

    private String type;
    private Date issueAt;
    private String issuer;
    private Object data;
}
