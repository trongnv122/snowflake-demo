package com.example.snowflake.dto;

import lombok.Data;

@Data
public class RegisterUserRequest {
    private String username;
    private String firstName;
    private String lastName;
    private String displayName;
    private String password;
}
