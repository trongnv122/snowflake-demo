package com.example.snowflake;

import com.example.snowflake.entity.UserEntity;
import com.example.snowflake.repository.UserRepository;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CustomerApplication.class)
class CustomerApplicationTests {
	@Autowired
	private UserRepository userRepo;

	@Test
	void contextLoads() {
	}

	@Test
	public void testDataSource() {
		Optional<UserEntity> userOpt = userRepo.findById(1L);
		Assert.assertTrue(userOpt.isPresent());
	}

}
