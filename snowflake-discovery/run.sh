#!/bin/bash

docker build -t trongnv/mics-discovery:1.0 \
        --build-arg MODULE=discovery --build-arg SRC=. --build-arg PROFILE=dev --build-arg PORT=8504  .

docker stack deploy -c docker-compose.yml snowflake-demo