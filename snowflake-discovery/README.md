# TEST PROJECT FOR SNOWFLAKE DATA CLOUD

### SETUP 
    - JDK 8
    - Spring boot 2.7.3
    - Hibernate, Lombok
### Goals:
    - Emit message via kafka

### KAFKA MESSAGE
- UI for Kafka message broker
```
docker run -p 8080:8080 \
	-e KAFKA_CLUSTERS_0_NAME=local \
	-e KAFKA_CLUSTERS_0_BOOTSTRAPSERVERS=localhost:9092 \
	-d provectuslabs/kafka-ui:latest

```

### NOTES
```
```

